# README #

This architecture framework provides the force.com technical architects and the developers to streamline the event handling on the triggers and provides a means to control the reentrant code. To learn more about this architecture framework, check out my blog post titled An architecture framework to handle triggers in the Force.com platform

![triggerdesign-v1-02.jpg](https://bitbucket.org/repo/RydbG5/images/3149592529-triggerdesign-v1-02.jpg)